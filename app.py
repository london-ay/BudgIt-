from budget import Budget

class App():
    def __init__(self,budgets):
        self.budgets=budgets
    def get_balance_by_category(self,category):
        balance = 0
        for budget in self.budgets:
            if budget.category == category:
                balance += budget.balance
        return balance
    def add_budget(self, budget):
        self.budgets.append(budget)

budgets = [Budget("food", 600), Budget("travel", 300), Budget("entertainment", 200)]
app = App(budgets)
print(app.get_balance_by_category("food"))


run = True
while run:
    print(app.budgets)
    control = input("Enter option 1 to create a budget, option 2 to add funds to a budget, or withdraw funds from a budget")
    if control == "1":
        new_budget_category = input("Enter category")
        new_budget_balance = input("Enter balance")
        new_budget = Budget(new_budget_category, new_budget_balance)
        app.add_budget(new_budget)
        print(app.budgets)