class Budget():
    def __init__(self,category, balance):
        self.category=category
        self.balance=balance

    def __repr__(self) -> str:
        return f"{self.category}-{self.balance}"

    def deposit(self,amount):
        self.balance += amount
        return self.balance

    def withdraw(self,amount):
        self.balance -= amount
        return self.balance

    def check_balance(self,amount):
        if self.balance >amount:
            return True
        else:
            return False